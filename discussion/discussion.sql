

INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Rivermaya");

-- To insert albums in the albums table:
INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 2);

-- insert songs
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Ice Cream", "00:04:16", "Kpop", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("You'll Never Know", "00:03:59", "Kpop", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", "00:03:54", "OPM", 2);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisipmata", "00:04:16", "OPM", 2);

-- [SECTION] Read/Sekect

-- Display the title and genre of all the songs.
SELECT song_name, genre FROM songs;

-- Display  the title of all the fields in songs table.
SELECT * FROM songs;

-- Display the title of all the songs with "OPM" genre;
SELECT song_name FROM songs WHERE genre = "OPM";

SELECT song_name, length FROM songs WHERE length > "00:04:00" AND genre = "Kpop";

-- [SECTION] update records
UPDATE songs SET length = "00:04:00" WHERE song_name = "You'll Never Know";

-- Removingthe where clause will update all rows/records
UPDATE songs SET length = "00:04:00";


-- [SECTION] Deleting Records
DELETE FROM songs WHERE genre = "Kpop" AND length > "00:04:00";

-- Removing the where clause will delete all the rows/records
DELETE FROM songs;